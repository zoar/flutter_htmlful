import 'package:flutter/material.dart';

import 'Style.dart';

class Div extends StatelessWidget {
  late Style style;
  dynamic body;

  InkWell? inkWell;

  /// style: Style 或者 Style[]
  Div({
    super.key,
    this.body,
    dynamic style,
    this.inkWell,
  }) {
    if (style is List<Style>) {
      var temp = Style();
      for (Style o in style) {
        temp.merge(o);
      }
      this.style = temp;
    } else if (style is Style) {
      this.style = style;
    } else if (style == null) {
      this.style = Style();
    } else {
      throw Exception('style 类型错误，只能是Style 或者 Style[]');
    }
    this.style.completionDefault();
  }

  @override
  Widget build(BuildContext context) {
    var c;
    if (body is Widget) {
      c = body;
    } else if (body is List<Widget>) {
      if (style!.wrap!) {
        c = Wrap(
          direction: style!.direction as Axis,
          textDirection: style!.textDirection,
          verticalDirection: style!.verticalDirection as VerticalDirection,
          alignment: style!.alignment as WrapAlignment,
          runAlignment: style!.runAlignment as WrapAlignment,
          crossAxisAlignment: style!.wrapCrossAlignment as WrapCrossAlignment,
          spacing: style!.spacing as double,
          runSpacing: style!.runSpacing as double,
          children: body as List<Widget>,
        );
      } else {
        c = Flex(
          direction: style!.direction as Axis,
          mainAxisAlignment: style!.mainAxisAlignment as MainAxisAlignment,
          mainAxisSize: style!.mainAxisSize as MainAxisSize,
          crossAxisAlignment: style!.crossAxisAlignment as CrossAxisAlignment,
          textDirection: style!.textDirection,
          verticalDirection: style!.verticalDirection as VerticalDirection,
          textBaseline: style!.textBaseline,
          children: body as List<Widget>,
        );
        if (style!.isIntrinsicHeight as bool) {
          c = IntrinsicHeight(child: c);
        }
        if (style!.isIntrinsicWidth as bool) {
          c = IntrinsicWidth(child: c);
        }
      }
    }

    if (style!.scrollable as bool) {
      c = SingleChildScrollView(
        scrollDirection: style!.direction as Axis,
        child: c,
      );
    }

    BorderRadius? borderRadius = style!.radius == 0
        ? BorderRadius.only(
      topLeft: Radius.circular(style!.radiusTopLeft as double),
      topRight: Radius.circular(style!.radiusTopRight as double),
      bottomRight: Radius.circular(style!.radiusBottomRight as double),
      bottomLeft: Radius.circular(style!.radiusBottomLeft as double),
    )
        : BorderRadius.circular(style!.radius as double);
    var decoration = BoxDecoration(
      color: style!.backgroundColor,
      gradient: style!.backgroundGradient,
      backgroundBlendMode: style!.backgroundBlendMode,
      borderRadius: borderRadius,
      image: style!.backgroundImage == null
          ? null
          : DecorationImage(
        image: style!.backgroundImage as ImageProvider<Object>,
        fit: style!.fit,
      ),
      boxShadow: style!.boxShadow,
      border: style!.border,
    );

    //最外层容器
    c = Container(
      height: style!.height,
      width: style!.width,
      clipBehavior: style!.clipBehavior as Clip,
      padding: style!.padding != 0
          ? EdgeInsets.all(style!.padding as double)
          : EdgeInsets.fromLTRB(
        style!.paddingLeft as double,
        style!.paddingTop as double,
        style!.paddingRight as double,
        style!.paddingBottom as double,
      ),
      decoration: inkWell == null ? decoration : null,
      child: c,
    );

    if (inkWell != null) {
      c = Material(
        color: Colors.transparent,
        child: Ink(
          decoration: decoration,
          child: InkWell(
            onTap: inkWell!.onTap,
            onLongPress: inkWell!.onLongPress,
            onSecondaryTap: inkWell!.onSecondaryTap,
            borderRadius: borderRadius,
            child: c,
          ),
        ),
      );
    }
    // 点击的涟漪效果不能包含margin区域，所以需要再包一层容器
    if (style!.margin != 0 || style!.marginTop != 0 || style!.marginLeft != 0 || style!.marginRight != 0 || style!.marginBottom != 0) {
      c = Container(
        height: style!.height,
        width: style!.width,
        margin: style!.margin != 0
            ? EdgeInsets.all(style!.margin as double)
            : EdgeInsets.fromLTRB(
          style!.marginLeft as double,
          style!.marginTop as double,
          style!.marginRight as double,
          style!.marginBottom as double,
        ),
        child: c,
      );
    }
    if (style!.flexGrow != 0) {
      c = Expanded(
        flex: style!.flexGrow as int,
        child: c,
      );
    }
    return c;
  }
}
