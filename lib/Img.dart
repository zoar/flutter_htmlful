import 'package:flutter/material.dart';

import 'Style.dart';

class Img extends StatelessWidget {
  late Style style;
  String src;
  String? errorSrc;

  InkWell? inkWell;

  Img({
    super.key,
    required this.src,
    this.errorSrc,
    dynamic style,
    this.inkWell,
  }) {
    if (style is List<Style>) {
      var temp = Style();
      for (Style o in style) {
        temp.merge(o);
      }
      this.style = temp;
    } else if (style is Style) {
      this.style = style;
    } else if (style == null) {
      this.style = Style();
    } else {
      throw Exception('style 类型错误，只能是Style 或者 Style[]');
    }
    this.style.completionDefault();
  }

  @override
  Widget build(BuildContext context) {
    BorderRadius? borderRadius = style!.radius == 0
        ? BorderRadius.only(
      topLeft: Radius.circular(style!.radiusTopLeft as double),
      topRight: Radius.circular(style!.radiusTopRight as double),
      bottomRight: Radius.circular(style!.radiusBottomRight as double),
      bottomLeft: Radius.circular(style!.radiusBottomLeft as double),
    )
        : BorderRadius.circular(style!.radius as double);
    Widget c;
    var errorBuilder = errorSrc == null
        ? null
        : (context, object, stack) {
      return Image.asset(
        errorSrc!,
        height: style!.height,
        width: style!.width,
        fit: style!.fit,
      );
    };
    if (src.startsWith('https://') || src.startsWith('http://')) {
      c = Image.network(
        src,
        height: style!.height,
        width: style!.width,
        fit: style!.fit,
        errorBuilder: errorBuilder,
      );
    } else {
      c = Image.asset(
        src,
        height: style!.height,
        width: style!.width,
        fit: style!.fit,
        errorBuilder: errorBuilder,
      );
    }

    if (style!.radius != 0) {
      c = ClipRRect(
        borderRadius: borderRadius,
        child: c,
      );
    }
    if (style!.width != null ||
        style!.margin != 0 ||
        style!.marginTop != 0 ||
        style!.marginLeft != 0 ||
        style!.marginRight != 0 ||
        style!.marginBottom != 0 ||
        style!.padding != 0 ||
        style!.paddingLeft != 0 ||
        style!.paddingTop != 0 ||
        style!.paddingRight != 0 ||
        style!.paddingBottom != 0) {
      c = Container(
        height: style!.height,
        width: style!.width,
        margin: style!.margin != 0
            ? EdgeInsets.all(style!.margin as double)
            : EdgeInsets.fromLTRB(
          style!.marginLeft as double,
          style!.marginTop as double,
          style!.marginRight as double,
          style!.marginBottom as double,
        ),
        padding: style!.padding != 0
            ? EdgeInsets.all(style!.padding as double)
            : EdgeInsets.fromLTRB(
          style!.paddingLeft as double,
          style!.paddingTop as double,
          style!.paddingRight as double,
          style!.paddingBottom as double,
        ),
        decoration: BoxDecoration(
          border: style!.border,
          borderRadius: borderRadius,
        ),
        child: c,
      );
    }
    if (inkWell != null) {
      c = Material(
        color: Colors.transparent,
        child: InkWell(
          onTap: inkWell!.onTap,
          onLongPress: inkWell!.onLongPress,
          onSecondaryTap: inkWell!.onSecondaryTap,
          borderRadius: borderRadius,
          child: c,
        ),
      );
    }
    if (style!.flexGrow != 0) {
      c = Expanded(
        flex: style!.flexGrow as int,
        child: c,
      );
    }
    return c;
  }
}
