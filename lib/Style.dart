import 'package:flutter/cupertino.dart';

class Style {
  double? width;
  double? height;

  ///背景色；如果设置了 backgroundGradient 或 backgroundImage 将会无效
  Color? backgroundColor;
  Gradient? backgroundGradient;
  BlendMode? backgroundBlendMode;
  ImageProvider<Object>? backgroundImage;
  BoxFit? fit;
  List<BoxShadow>? boxShadow;
  BoxBorder? border;

  ///是否可以滚动，滚动方向同 direction
  bool? scrollable;

  ///超出布局部分的处理
  Clip? clipBehavior;

  double? radius;
  double? radiusTopLeft;
  double? radiusTopRight;
  double? radiusBottomLeft;
  double? radiusBottomRight;
  double? margin;
  double? marginTop;
  double? marginLeft;
  double? marginRight;
  double? marginBottom;
  double? padding;
  double? paddingTop;
  double? paddingLeft;
  double? paddingRight;
  double? paddingBottom;

  /// 布局方向，默认垂直：Axis.vertical
  Axis? direction;

  ///flex布局才生效的相关属性
  MainAxisAlignment? mainAxisAlignment;
  MainAxisSize? mainAxisSize;
  CrossAxisAlignment? crossAxisAlignment;

  TextDirection? textDirection;
  VerticalDirection? verticalDirection;
  TextBaseline? textBaseline;

  ///同 flex 布局的 Expanded，占据剩余空间的比例，Div 的 body 为数组时有效
  int? flexGrow;

  ///高度自适应内容
  bool? isIntrinsicHeight;
  bool? isIntrinsicWidth;

  ///是否为流式布局，以及流式布局才生效的相关属性
  bool? wrap;
  double? spacing;
  double? runSpacing;
  WrapAlignment? alignment;
  WrapAlignment? runAlignment;
  WrapCrossAlignment? wrapCrossAlignment;

  Style({
    this.height,
    this.width,
    this.backgroundColor,
    this.backgroundGradient,
    this.backgroundBlendMode,
    this.backgroundImage,
    this.fit,
    this.boxShadow,
    this.border,
    this.scrollable,
    this.clipBehavior,
    this.radius,
    this.radiusBottomLeft,
    this.radiusBottomRight,
    this.radiusTopLeft,
    this.radiusTopRight,
    this.margin,
    this.marginTop,
    this.marginLeft,
    this.marginBottom,
    this.marginRight,
    this.padding,
    this.paddingBottom,
    this.paddingLeft,
    this.paddingRight,
    this.paddingTop,
    this.direction,
    this.mainAxisAlignment,
    this.mainAxisSize,
    this.crossAxisAlignment,
    this.textDirection,
    this.verticalDirection,
    this.textBaseline,
    this.flexGrow,
    this.wrap,
    this.spacing,
    this.runSpacing,
    this.alignment,
    this.runAlignment,
    this.wrapCrossAlignment,
    this.isIntrinsicHeight,
    this.isIntrinsicWidth,
  });

  merge( Style style) {
    this.height = style.height ?? this.height;
    this.width = style.width ?? this.width;
    this.backgroundColor = style.backgroundColor ?? this.backgroundColor;
    this.backgroundGradient = style.backgroundGradient ?? this.backgroundGradient;
    this.backgroundBlendMode = style.backgroundBlendMode ?? this.backgroundBlendMode;
    this.backgroundImage = style.backgroundImage ?? this.backgroundImage;
    this.fit = style.fit ?? this.fit;
    this.boxShadow = style.boxShadow ?? this.boxShadow;
    this.border = style.border ?? this.border;
    this.scrollable = style.scrollable ?? this.scrollable;
    this.clipBehavior = style.clipBehavior ?? this.clipBehavior;
    this.radius = style.radius ?? this.radius;
    this.radiusBottomLeft = style.radiusBottomLeft ?? this.radiusBottomLeft;
    this.radiusBottomRight = style.radiusBottomRight ?? this.radiusBottomRight;
    this.radiusTopLeft = style.radiusTopLeft ?? this.radiusTopLeft;
    this.radiusTopRight = style.radiusTopRight ?? this.radiusTopRight;
    this.margin = style.margin ?? this.margin;
    this.marginTop = style.marginTop ?? this.marginTop;
    this.marginLeft = style.marginLeft ?? this.marginLeft;
    this.marginBottom = style.marginBottom ?? this.marginBottom;
    this.marginRight = style.marginRight ?? this.marginRight;
    this.padding = style.padding ?? this.padding;
    this.paddingTop = style.paddingTop ?? this.paddingTop;
    this.paddingBottom = style.paddingBottom ?? this.paddingBottom;
    this.paddingLeft = style.paddingLeft ?? this.paddingLeft;
    this.paddingRight = style.paddingRight ?? this.paddingRight;
    this.direction = style.direction ?? this.direction;
    this.mainAxisAlignment = style.mainAxisAlignment ?? this.mainAxisAlignment;
    this.mainAxisSize = style.mainAxisSize ?? this.mainAxisSize;
    this.crossAxisAlignment = style.crossAxisAlignment ?? this.crossAxisAlignment;
    this.textDirection = style.textDirection ?? this.textDirection;
    this.verticalDirection = style.verticalDirection ?? this.verticalDirection;
    this.textBaseline = style.textBaseline ?? this.textBaseline;
    this.flexGrow = style.flexGrow ?? this.flexGrow;
    this.isIntrinsicHeight = style.isIntrinsicHeight ?? this.isIntrinsicHeight;
    this.isIntrinsicWidth = style.isIntrinsicWidth ?? this.isIntrinsicWidth;
    this.wrap = style.wrap ?? this.wrap;
    this.spacing = style.spacing ?? this.spacing;
    this.runSpacing = style.runSpacing ?? this.runSpacing;
    this.alignment = style.alignment ?? this.alignment;
    this.runAlignment = style.runAlignment ?? this.runAlignment;
    this.wrapCrossAlignment = style.wrapCrossAlignment ?? this.wrapCrossAlignment;
  }

  ///补全默认值
  completionDefault() {
    scrollable = scrollable ?? false;
    clipBehavior = clipBehavior ?? Clip.none;
    radius = radius ?? 0;
    radiusBottomLeft = radiusBottomLeft ?? 0;
    radiusBottomRight = radiusBottomRight ?? 0;
    radiusTopLeft = radiusTopLeft ?? 0;
    radiusTopRight = radiusTopRight ?? 0;
    margin = margin ?? 0;
    marginTop = marginTop ?? 0;
    marginLeft = marginLeft ?? 0;
    marginBottom = marginBottom ?? 0;
    marginRight = marginRight ?? 0;
    padding = padding ?? 0;
    paddingTop = paddingTop ?? 0;
    paddingBottom = paddingBottom ?? 0;
    paddingLeft = paddingLeft ?? 0;
    paddingRight = paddingRight ?? 0;
    direction = direction ?? Axis.vertical;
    mainAxisAlignment = mainAxisAlignment ?? MainAxisAlignment.start;
    mainAxisSize = mainAxisSize ?? MainAxisSize.max;
    crossAxisAlignment = crossAxisAlignment ?? CrossAxisAlignment.start;
    verticalDirection = verticalDirection ?? VerticalDirection.down;
    flexGrow = flexGrow ?? 0;
    isIntrinsicHeight = isIntrinsicHeight ?? false;
    isIntrinsicWidth = isIntrinsicWidth ?? false;
    wrap = wrap ?? false;
    spacing = spacing ?? 0;
    runSpacing = runSpacing ?? 0;
    alignment = alignment ?? WrapAlignment.start;
    runAlignment = runAlignment ?? WrapAlignment.start;
    wrapCrossAlignment = wrapCrossAlignment ?? WrapCrossAlignment.start;
  }
}